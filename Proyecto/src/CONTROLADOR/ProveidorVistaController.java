package CONTROLADOR;

import java.time.LocalDate;

import MODEL.Adreca;
import MODEL.PersonaAbstract;
import MODEL.Proveidor;

public class ProveidorVistaController {

	//CONSTRUCTOR OBJETO VISTA CONTROLLER
	private ProveidorsDAO provd;
		public ProveidorVistaController(ProveidorsDAO provd) {//ya no hace falta pasar el dao a todas las funciones
			this.provd = provd;
		}
	//inici
	public void inici (){
	int opcio = 0;
	int idpersona;
	boolean seguir = true;
	do {
	menu();
	opcio = UtilConsole.pedirInt();
	switch(opcio) {
	case 0:
		seguir = false;
		break;
	case 1:
		
		afegirProveidor(10,false);//pasamos false a modificar
		break;
	case 2:
		System.out.println("Escriu el ID del proveidor que vols buscar: ");
		idpersona = UtilConsole.pedirInt();
		Proveidor provtemp = provd.buscar(idpersona);
		if (provd != null) {//lo buscamos
			System.out.println(provtemp);//si lo encuentra, lo imprime
		} else {//sino
			System.out.println("El producte no existeix.");
		}
		break;
	case 3:
		System.out.println("Escriu l'ID del proveidor que vols modificar: ");
		idpersona = UtilConsole.pedirInt();
		PersonaAbstract ptemp = provd.buscar(idpersona);
		if (ptemp != null) {
			afegirProveidor( idpersona, true);

		} else {
			System.out.println("El producte no existeix.");
		}
		break;
	case 4:
		System.out.println("Quin client vols eliminar? Escriu el seu ID:");
		idpersona = UtilConsole.pedirInt();
		provd.eliminar(idpersona);
		break;
	case 5:
		mostrar();
		break;
	default:
		break;
	}
	}while(seguir);
	
	}


private void afegirProveidor(int idpersona, boolean modificar) {
	Proveidor proAbs = null;
	if(!modificar) {
		System.out.println("ID: ");
		 idpersona = UtilConsole.pedirInt();
	}

	System.out.println("DNI: ");
	String dNI = UtilConsole.pedirDNI();
	System.out.println("Nom: ");
	String nom = UtilConsole.pedirString();
	System.out.println("Cognoms: ");
	String cognoms = UtilConsole.pedirString();
	System.out.println("Data de naixement:");
	LocalDate dataNaixement = UtilConsole.pedirFecha();
	System.out.println("Email:");
	String email = UtilConsole.pedirEmail();
	System.out.println("Telefon:");
	String telefon = UtilConsole.pedirTelefono();
	System.out.println("Direccio:");
	Adreca adreca = IniciVistaController.afegirAdreca();
	System.out.println("Descompte:");
	double dtoProntoPago = UtilConsole.pedirDouble();
	proAbs = new Proveidor(idpersona,dNI, nom,cognoms , dataNaixement, email,telefon,adreca, dtoProntoPago );
	if (proAbs != null) {
		provd.guardar(proAbs);
	} 
	}

private void mostrar() {
	IniciVistaController.imprimir(provd);
		
	}

private static void menu() {
	System.out.println("0. Sortir" + "\n" + "1. Introduir proveidor" + "\n" + "2. Buscar proveidor" + "\n"
			+ "3. Modificar proveidor" + "\n" + "4. Eliminar proveidor" + "\n" + "5. Mostrar tots els proveidors"
			+ "\n" + "Opcio:");

}
} 

