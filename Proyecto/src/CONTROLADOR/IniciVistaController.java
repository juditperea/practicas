package CONTROLADOR;

import java.util.HashMap;

import MODEL.Adreca;
import MODEL.Persistable;
import MODEL.ProductesDAO;

public class IniciVistaController {

	public static void main(String[] args) {
		
		int opcio = 0;//inicializamos variable de opcion, que pediremos en el menu
		boolean seguir = true;
	
		ProductesDAO pd = new ProductesDAO();//creamos el objeto DAO 
		pd.obrirFitxer();//CUANDO EJECUTAMOS EL PROGRAMA ABRE EL ARCHIVO1
		ProductesVistaController pvc = new ProductesVistaController(pd);//creamos el objeto con el q llamaremos a ProductesVistaController, alli tenemos el constructor
		ClientsDAO cd = new ClientsDAO();//creamos el objeto DAO 
		ClientVistaController cvc = new ClientVistaController(cd);//creamos el objeto con el q llamaremos a ProductesVistaController, alli tenemos el constructor
		ProveidorsDAO provd = new ProveidorsDAO();//creamos el objeto DAO 
		ProveidorVistaController prvc = new ProveidorVistaController(provd);//creamos el objeto con el q llamaremos a ProductesVistaController, alli tenemos el constructor
		
	
		do {
			//menu
			System.out.println("0. Sortir");
			System.out.println("1. Productes");
			System.out.println("2. Clients");
			System.out.println("3. Proveidors");
			opcio = UtilConsole.pedirInt();//pedimos opcion para elegir a que submenu queremos ir
			//segun lo que se escoge, se llama al submenu del controlador que toca
			if (opcio == 1) {
				pvc.inici();//nos envia a ProductesVistaController y le pasamos el objeto DAO que hemos creado
			}else  if (opcio ==2){
				cvc.inici();//nos envia a ClientsVistaController y le pasamos el objeto DAO que hemos creado
			}else  if (opcio == 3){
				prvc.inici();//nos envia a ProveidorsVistaController y le pasamos el objeto DAO que hemos creado
			}else if (opcio == 0) {
				pd.guardarFitxer();//CUANDO SALIMOS DEL PROGRAMA GUARDA EN UN ARCHIVO LOS DATOS
				seguir = false;
			}

		} while (seguir);	//salimos con un 0
		
	}
	public static  Adreca afegirAdreca(){//funcion para añadir la direccion 
		System.out.println("Poblacio: ");
		String poblacio = UtilConsole.pedirString();
		System.out.println("Provincia: ");
		String provincia = UtilConsole.pedirString();
		System.out.println("CP: ");
		String cp = UtilConsole.pedirCP();
		System.out.println("Domicili: ");
		String domicili = UtilConsole.pedirString();
		Adreca adreca = new Adreca(poblacio, provincia, cp, domicili);//creamos un objeto de tipo Adreca
		return adreca;//nos devuelve la direccion, que le pasaremos al metodo de Añadir cuando tengamos que añadir un Cliente o Proveidor

	}
	public static <T> void imprimir(Persistable<T> p) {//imprime cualquier hashmap ya que es una funcion generica en la interfaz

		HashMap<?, ?> mapa = p.getMap();//con el objeto que le pasamos desde la funcion del DAO, buscamos en el mapa
		if (mapa == null) {//si el mapa esta vacío
			System.out.println("No hi ha dades per mostrar");
			return;//return null
		}
		for (HashMap.Entry<?, ?> clau : mapa.entrySet()) {//itera en el hashmap buscando las llaves y sus valores
			System.out.println("ID: " + clau.getKey() + " Valor: " + clau.getValue());//imprime mapa
		}


	}



}
