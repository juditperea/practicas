package MODEL;
import java.util.Map;


public interface ImportExportable<T> {
//metodes
	public Map<Integer, T> importar(String nomFitxer); 
	
	public boolean exportar(String nomFitxer, Persistable<T> dao);

	
	
	
	
	
}
