package MODEL;

public class Adreca {
	// atributs

	private String poblacio;

	private String provincia;

	private String cp;

	private String domicili;

	// getters i setters

	public String getPoblacio() {
		return poblacio;
	}

	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}

	public  String getProvincia() {
		return provincia;
	}

	public  void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public  String getCp() {
		return cp;
	}

	public  void setCp(String cp) {
		this.cp = cp;
	}

	public  String getDomicili() {
		return domicili;
	}

	public  void setDomicili(String domicili) {
		this.domicili = domicili;
	}

	// constructors

	public Adreca(String poblacio, String provincia, String cp, String domicili) {
		this.poblacio = poblacio;
		this.provincia = provincia;
		this.cp = cp;
		this.domicili = domicili;
	}

	
	//metode toString
	
	@Override
	public String toString() {
		return "Adreca [Poblacio = " + poblacio + ", Provincia = " + provincia + ", Codi Postal = " + cp + ", Domicili = " + domicili
				+ "]";
	}

}

