package MODEL;

import java.time.LocalDate;

public class Client extends PersonaAbstract{//hereda de PersonaAbstract

	//variables
	
	boolean enviarPublicitat;

	//constructores 
	
	public Client() {
		super();
		enviarPublicitat = false;
	}

	public Client(int idpersona, String dNI, String nom, String cognoms, LocalDate dataNaixement, String email,
			String telefon, Adreca adreca, boolean enviarPublicitat) {
		super(idpersona, dNI, nom, cognoms, dataNaixement, email, telefon, adreca);
		this.enviarPublicitat = enviarPublicitat;
	}

	

	//getters i setters
	public boolean getEnviarPublicitat() {
		return enviarPublicitat;
	}

	public void setEnviarPublicitat(boolean enviarPublicitat) {
		this.enviarPublicitat = enviarPublicitat;
	}
	//toString

	@Override
	public String toString() {
		return "Client [enviarPublicitat=" + getEnviarPublicitat()
				+ ", Id =" + getIdPersona() + ", DNI=" + getDNI() + ", Nom=" + getNom()
				+ ", Cognoms=" + getCognoms() + ", Data de naixement =" + getDataNaixement() + ", Email="
				+ getEmail() + ", Telefon=" + getTelefon() + ", Direccio=" + getAdreca() + ", Edat="
				+ getEdad() + "]";
	}
	
	
}
