package MODEL;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class ImportExportData implements ImportExportable<ProducteAbstract> {

	@Override
	public Map<Integer, ProducteAbstract> importar(String nomFitxer) {// nos devolvera un hashmap de producto,le pasamos
																		// el nombre del archivo
		Map<Integer, ProducteAbstract> productes = new HashMap<>();// creamos un mapa temporal de productos
		try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("res/" + nomFitxer + ".dat")))) {
			while (dis.available() > 0) { // leer todos los productos del archivo
				int IdProducte = dis.readInt();
				String Nom = dis.readUTF();
				System.out.println(Nom);

				double PreuVenda = dis.readDouble();
				int Stock = dis.readInt();
				System.out.println("Id: " + IdProducte);
				System.out.println("Nom : " + Nom);
				System.out.println("Preu : " + PreuVenda);
				System.out.println("Stock : " + Stock);
				Producte prod = new Producte(IdProducte, Nom, PreuVenda, Stock);
				productes.put(prod.getIdproducte(), prod);
			}
			return productes;// devuelve el mapa de productos
		} catch (Exception e) {
			System.out.println(e.getMessage());// si hay algun error, salta el mensaje
		}
		return productes;
	}

	@Override
	public boolean exportar(String nomFitxer, Persistable<ProducteAbstract> dao) {
		try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("res/" + nomFitxer + ".dat")))) {
			// creamos el objeto DataOutPutStream y FileOutputStream y le pasamos el nombre
			// del fichero que queremos escribir
			for (ProducteAbstract producto : dao.getMap().values()) {// iteramos en el mapa que le estamos pasando con
				System.out.println(producto);										// el dao
				if (producto instanceof Producte) {// si producto es un Producto(y no un Pack)

					String productoString = producto.toFileFormat() +
					System.lineSeparator();//para que cada producto se escriba en una linea
					dos.writeInt(producto.getIdproducte());
					dos.writeUTF(producto.getNom());
					dos.writeDouble(((Producte) producto).getPreuVenda());
					dos.writeInt(((Producte) producto).getStock());
				}
			}

			return true;// devuelve true si se ha exportado correctamente
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;// devuelve false y el nombre del error si algo falla
		}
	}

}
