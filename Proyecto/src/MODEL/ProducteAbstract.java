package MODEL;

public abstract class ProducteAbstract {//Clase abstracta
	// variables

	private int idproducte;
	private String nom;

	// constructors
	public ProducteAbstract() {
		super();
	}

	public ProducteAbstract(int idproducte, String nom) {
		super();
		this.idproducte = idproducte;
		this.nom = nom;
	}

	// getters i setters
	public int getIdproducte() {
		return idproducte;
	}

	public void setIdproducte(int idproducte) {
		this.idproducte = idproducte;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}



	// toString


	@Override
	public String toString() {
		return "Producte [ID =" + idproducte + ", Nom =" + nom  + "]";
	}

	// override del mètode equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)// en el cas de que siguin iguals els objectes retorna true
			return true;
		if (obj == null)// si el objecte no existeix retorna false
			return false;
		ProducteAbstract other = (ProducteAbstract) obj;
		if (this.getNom().equals(other.getNom()))// si el nom es igual al nom de l'objecte que li donem
			return true;// retorna true>
		return false;// si no es cap de les anteriors retorna false
	}

	public abstract String toFileFormat();


}
