package MODEL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import CONTROLADOR.UtilConsole;

public final class Pack extends ProducteAbstract implements Serializable{//hereda de ProducteAbstract
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// variables
	private ArrayList<Producte> productes;
	double percentatgeDescompte;

	// constructors
	public Pack() {
		super();
	
	}

	public Pack(int idproducte, String nom,ArrayList<Producte> productes, double percentatgeDescompte) {
		super(idproducte, nom);
		this.productes = productes;
		this.percentatgeDescompte = percentatgeDescompte;
	}

	// getters i setters
	public ArrayList<Producte> getProductes() {
		return productes;
	}
	public double getPercentatgeDescompte() {
		return percentatgeDescompte;
	}

	public void setPercentatgeDescompte(double percentatgeDescompte) {
		this.percentatgeDescompte = percentatgeDescompte;
	}

	// metodes
	public void afegirProducte(ProducteAbstract prAbs) {
		productes.add((Producte) prAbs);
	}

	public void esborrarProducte() {
		System.out.println("Escriu l'id del producte que vols eliminar: ");
		int idproducte = UtilConsole.pedirInt();
		productes.remove(idproducte);
	}

	@Override
	public String toString() {
		String IdsProductes = "";
		for (ProducteAbstract producte : productes) {
			IdsProductes += producte.getIdproducte() + " | ";
		}
		return super.toString() + " Pack [ID's dels productes: " + IdsProductes + "]";
	}

	// override del metode equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)// en el cas de que siguin iguals els objectes retorna true
			return true;
		if (obj == null)// si el objecte no existeix retorna false
			return false;
		return this.productes.equals(this.productes);// compara el pack actual de productos
		// que le estamos pasando con el que cassteamos
	}

	@Override
	public String toFileFormat() {
		  return getIdproducte() + "|" + getNom() + "|";
		
	}

	

}
