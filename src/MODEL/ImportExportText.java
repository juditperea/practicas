package MODEL;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ImportExportText implements ImportExportable<ProducteAbstract> {

	/* @Override
    public Map<Integer, ProducteAbstract> importar(String nomFitxer) {
        Map<Integer, ProducteAbstract> productes = new HashMap<>();
        Producte prod;
     File file = new File("res/" + nomFitxer + ".txt");//creamos un objeto de tipo File(archivo) y le pasamos como parámetro
        try(Scanner sc = new Scanner(new BufferedReader(new FileReader(file)))) {
            //el nombre de la carpeta de la ruta(res) + variable nombreArchivo que le pasaremos desde el Vista Controller de Productos 
            //diciendole el nombre que tendra el archivo + ".txt" ya que será un archivo .txt
            while (sc.hasNextLine()) {//mientras haya mas lineas
                String[] split = sc.nextLine().split("\\|");//lee la siguiente y separa el contenido por | y lo mete en un array de strings 
                Integer id = Integer.valueOf(split[0]);//el id lo parsea ya que era un string y lo mete en la primera posicion del array
                String nombre = split[1];//lo mete en la segunda posicion, no tiene que parsear porque es string
                Double preuvenda = Double.valueOf(split[2]);//lo mete en la tercera posicion y parsea
                Integer stock = Integer.valueOf(split[3]);//lo mete en la cuarta posicion y parsea
                prod = new Producte(id, nombre, preuvenda, stock);//creamos nuevo objeto de tipo producto
                productes.put(prod.getIdproducte(), prod);//cogemos con un get el id de producto, guardamos el producto con su id en el hashmap
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());//si algo falla salta el error
        }finally {
        	  sc.close();//cerramos scanner
        }
        return productes;//devuelve el hashmap
    }*/


	//CON BUFFERED READER
	@Override
	public Map<Integer, ProducteAbstract> importar(String nomFitxer) {
		Map<Integer, ProducteAbstract> productes = new HashMap<Integer, ProducteAbstract>();
		Producte prod;
		try(BufferedReader bfr = new BufferedReader(new FileReader("res/" + nomFitxer + ".txt"))) {
			//creamos un objeto de tipo BufferedReader y FileReader y le pasamos como parámetro el nombre del archivo
			String linea = "";
			//NumberFormat nformat = NumberFormat.getInstance(Locale.ITALIAN);//sirve para que el double acepte la coma y no el punto
			while ((linea = bfr.readLine()) != null) {//si la linea no esta vacia
				String[] split = linea.split("\\|");//lee la siguiente y separa el contenido por | y lo mete en un array de strings 
				Integer id = Integer.valueOf(split[0]);//el id lo parsea ya que era un string y lo mete en la primera posicion del array
				String nombre = split[1];//lo mete en la segunda posicion, no tiene que parsear porque es string
				Double preuvenda = Double.valueOf(split[2]);//lo mete en la tercera posicion y le da el formato local con su valor
				Integer stock = Integer.valueOf(split[3]);//lo mete en la cuarta posicion y parsea
				prod = new Producte(id, nombre, preuvenda, stock);//creamos nuevo objeto de tipo producto
				productes.put(prod.getIdproducte(), prod);//sacamos con un get el id de producto, guardamos el producto con su id en el hashmap
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());//si algo falla salta el error
		}
		return productes;//devuelve el hashmap
	}
	@Override
	public boolean exportar(String nomFitxer, Persistable<ProducteAbstract> dao) {
		try(BufferedWriter bw = new BufferedWriter(new FileWriter("res/" + nomFitxer + ".txt"))){
			//creamos un objeto de tipo File(archivo) y le pasamos como parámetro el nombre del archivo
			for (ProducteAbstract producto : dao.getMap().values()) {//iteramos en el mapa que le estamos pasando con el dao
				//sacamos el valor de cada producto que hay en el mapa
				bw.write(producto.toFileFormat());//escribimos con el bw en el archivo todos los productos con el formato del archivo
				//con la función que creamos en ProducteAbstract,Productos y Packs
				bw.newLine();//salto de linea
			}
			return true;//devuelve true si se ha hecho la exportación
		} catch (Exception e) {
			System.out.println("No esta funcionando correctamente.");
			return false;//devuelve false y nos informa con un mensaje si no ha funcionado bien
		}
	}
}

