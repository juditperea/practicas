package MODEL;

import java.util.HashMap;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ProductesDAO implements Persistable<ProducteAbstract> {//implementa la interficie Persistable

	private HashMap<Integer, ProducteAbstract> productes = new HashMap<Integer, ProducteAbstract>();//la misma lista guarda tanto productos como packs


	@Override
	public ProducteAbstract guardar(ProducteAbstract prod) {
		
			productes.put(prod.getIdproducte(), prod);//guarda el producto y lo devuelve
			return prod;
		}
	@Override
	public ProducteAbstract eliminar(int idproducte) {
		productes.get(idproducte);//busca el producto que tiene el id que le pasamos
		return productes.remove(idproducte);//lo elimina y devuelve el producto que hemos eliminado
	}

	@Override
	public ProducteAbstract buscar(int idproducte) {//busca el producto que tiene el id que le pasamos
			return productes.get(idproducte);//devuelve el producto
	}

	@Override
	public HashMap<Integer, ProducteAbstract> getMap() {//le pasamos el hashmap de ProductosAbstract a ProductesVistaController que le pasa a IniciVistaController
		if (productes == null) {//si el mapa esta vacio
			System.out.println("No hi ha dades per mostrar");
			return null;//devuelve null
		}
		return productes;//si no esta vacio, devuelve los productos/packs
	}

			
	public void guardarFitxer() {
		//FileOutputStream fos;
				try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("res/productes.dat"))){
					oos.writeObject(productes);
				}catch (Exception e) {
					System.out.println("Exception");
				}	
	}
	
	@SuppressWarnings("unchecked")
	public void obrirFitxer() {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("res/productes.dat"))){
			productes = (HashMap<Integer, ProducteAbstract>) ois.readObject();
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
			System.out.println("L'arxiu no existeix");
		}catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Exception");
		}
	}
}

