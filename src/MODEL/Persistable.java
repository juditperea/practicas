package MODEL;

import java.util.HashMap;

public interface Persistable <T>{//INTERFAZ (se implementa en cada DAO)
	
public T guardar(T obj);//guardar pide un objeto de cualquier tipo

public T eliminar(int id);//eliminar mediante un id de tipo int

public T buscar (int id);//buscar mediante un id de tipo int

public HashMap<Integer, T> getMap();//pide hashmap que tenga una KEY integer

}
