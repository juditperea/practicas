package CONTROLADOR;

import java.util.HashMap;

import MODEL.Client;
import MODEL.Persistable;

public class ClientsDAO implements Persistable<Client>{//implementa la interfaz Persistable
	private HashMap<Integer, Client> clients = new HashMap<Integer, Client>();
	

	public Client guardar(Client cl) {
		
			return clients.put(cl.getIdPersona(), cl);//guarda el cliente y lo devuelve
		}

	@Override
	public Client eliminar(int idPersona) {
		clients.get(idPersona);//busca el cliente que tiene el id que le pasamos
		return clients.remove(idPersona);//lo elimina y devuelve el cliente que hemos eliminado
	}

	@Override
	public Client buscar(int idPersona) {//busca el cliente que tiene el id que le pasamos
		return clients.get(idPersona);//devuelve el cliente
	}

	@Override
	public HashMap<Integer, Client> getMap() {//le pasamos a la funcion de getMap de Persistable el mapa de K Integer y V Client
		 if (clients == null) {//si el mapa está vacio
			    System.out.println("No hi ha dades per mostrar");
			    return null;//devuelve null
			  }
		return clients;//si encuentra clientes, los devuelve
	}
}
