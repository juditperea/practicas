package CONTROLADOR;

import java.time.LocalDate;

import MODEL.Adreca;
import MODEL.Client;
import MODEL.PersonaAbstract;

public class ClientVistaController {

	//CONSTRUCTOR OBJETO VISTA CONTROLLER 
	private ClientsDAO cd;
		public ClientVistaController(ClientsDAO cd) {//ya no hace falta pasar el dao a todas las funciones
			this.cd = cd;
		}
	//inici
	public void inici (){
		boolean seguir = true;
		int opcio = 0;
		do{
			menu();
			opcio = UtilConsole.pedirInt();
			switch(opcio) {
			case 0:
				seguir = false;
				break;
			case 1:
				afegirClient(10,false);//pasamos false a modificar
				break;
			case 2:
				System.out.println("Escriu el ID del client que vols buscar: ");
				int idpersona = UtilConsole.pedirInt();
				Client cltemp = cd.buscar(idpersona);//creamos una variable temporal para guardar el objeto que nos devuelve la busqueda
				if (cd != null) {//lo buscamos
					System.out.println(cltemp);//si lo encuentra, lo imprime

				} else {
					System.out.println("El producte no existeix.");
				}
				break;
			case 3:
				System.out.println("Escriu l'ID del client que vols modificar: ");
				idpersona = UtilConsole.pedirInt();
				PersonaAbstract ptemp = cd.buscar(idpersona);
				if (ptemp != null) {
					afegirClient( idpersona, true);

				} else {
					System.out.println("El producte no existeix.");
				}
				break;
			case 4:
				System.out.println("Quin client vols eliminar? Escriu el seu ID:");
				idpersona = UtilConsole.pedirInt();
				cd.eliminar(idpersona);
				break;
			case 5:
				mostrar();
				break;
			default:
				break;
			}
		}while(seguir);
	}
	


	private void afegirClient( int idpersona, boolean modificar) {
		Client clAbs = null;
		if(!modificar) {
			System.out.println("ID: ");
			 idpersona = UtilConsole.pedirInt();
		}

		System.out.println("DNI: ");
		String dNI = UtilConsole.pedirDNI();
		System.out.println("Nom: ");
		String nom = UtilConsole.pedirString();
		System.out.println("Cognoms: ");
		String cognoms = UtilConsole.pedirString();
		System.out.println("Data de naixement:");
		LocalDate dataNaixement = UtilConsole.pedirFecha();
		System.out.println("Email:");
		String email = UtilConsole.pedirEmail();
		System.out.println("Telefon:");
		String telefon = UtilConsole.pedirTelefono();
		System.out.println("Direccio:");
		Adreca adreca = afegirAdreca();//la direccion tanto aqui como en ProveidorVistaController se la pasamos desde la funcion de Inicio
		System.out.println("Enviar publicitat (true | false) :");
		boolean enviarPublicitat = UtilConsole.pedirBoolean();
		clAbs = new Client(idpersona,dNI, nom,cognoms , dataNaixement, email,telefon,adreca, enviarPublicitat );//creamos el Cliente
		if (clAbs != null) {//si el Cliente tiene datos
			cd.guardar(clAbs);//lo guardamos
		} 
	}
	public static  Adreca afegirAdreca(){//funcion para añadir la direccion 
		System.out.println("Poblacio: ");
		String poblacio = UtilConsole.pedirString();
		System.out.println("Provincia: ");
		String provincia = UtilConsole.pedirString();
		System.out.println("CP: ");
		String cp = UtilConsole.pedirCP();
		System.out.println("Domicili: ");
		String domicili = UtilConsole.pedirString();
		Adreca adreca = new Adreca(poblacio, provincia, cp, domicili);//creamos un objeto de tipo Adreca
		return adreca;//nos devuelve la direccion, que le pasaremos al metodo de Añadir cuando tengamos que añadir un Cliente o Proveidor

	}
	private void mostrar() {//imprime hashmap de clientes
		IniciVistaController.imprimir(cd);
		
	}

	private void menu() {
		System.out.println("0. Sortir" + "\n" + "1. Introduir client" + "\n" + "2. Buscar client" + "\n"
				+ "3. Modificar client" + "\n" + "4. Eliminar client" + "\n" + "5. Mostrar tots els clients"
				+ "\n" + "Opcio:");

	}
}
